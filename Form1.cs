﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Security;
using System.Text;
using System.Windows.Forms;

namespace encoderthingform
{
    public partial class Form1 : Form
    {
        public static string finaltxt;
        public static Bitmap sprite;
        //fuck im lazy lol
        struct logtoconsole
        {
            public static bool Checked = true;
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void dothing(object sender, EventArgs e)
        {
            Console.WriteLine(logtoconsole.Checked ? "logging everything to console.." : "not logging anything..");
            if (filediag.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    progressBar1.Value = 0;
                    donetxt.Text = "doing the thing";
                    sprite = new Bitmap(filediag.FileName);
                    Console.WriteLine(logtoconsole.Checked ? "loaded image.." : null);
                    int spritewidth;
                    int spriteheight;

                    spritewidth = checkBox1.Checked ? (int)realWidth.Value : sprite.Width;
                    spriteheight = checkBox1.Checked ? (int)realHeight.Value : sprite.Height;

                    progressBar1.Maximum = sprite.Width * sprite.Height + (int)sprFrames.Value;

                    finaltxt += 
                        "globalvar " + sprname.Text + ";" + 
                        "tempsurface=surface_create(" + sprite.Width.ToString() + "," + sprite.Height.ToString() + ");" +
                        "surface_set_target(tempsurface);draw_clear_alpha(0,0);";
                    double lastalpha = -1.0;
                    for(int xx = 0; xx < spritewidth; xx++)
                    {
                        for (int yy = 0; yy < spriteheight; yy++)
                        {
                            progressBar1.Value += 1;
                            int colour = sprite.GetPixel(xx, yy).ToArgb();
                            double a = ((colour >> 24) & 255) / 255.0;
                            if (a == 0.0)
                            {
                                continue;
                            }
                            Console.WriteLine(logtoconsole.Checked ? ("writting pixel " + xx.ToString() + "," + yy.ToString() + "..") : null);
                            int r = colour & 255;
                            int g = (colour >> 8) & 255;
                            int b = (colour >> 16) & 255;
                            string col = ((r << 16) + (g << 8) + b).ToString();
                            finaltxt += (lastalpha == a) ? null : "draw_set_alpha(" + a.ToString() + ");";
                            finaltxt += 
                                "draw_point_colour(" + xx.ToString() + "," + yy.ToString() + "," + col + ");";
                            lastalpha = a;
                        }
                    }
                    finaltxt += 
                        "draw_set_alpha(1);"+
                        "surface_reset_target();"+
                        sprname.Text + "=sprite_create_from_surface(tempsurface,0,0," + (sprite.Width / sprFrames.Value).ToString() + "," + sprite.Height.ToString() + ",0,0," + sprX.Value.ToString() + "," + sprY.Value.ToString() + ");";
                    for(int i = 1; i < sprFrames.Value; i++)
                    {
                        progressBar1.Value += 1;
                        Console.WriteLine(logtoconsole.Checked ? ("adding frame " + i.ToString() + "..") : null);
                        finaltxt +=
                            "sprite_add_from_surface(" + sprname.Text + ",tempsurface," + ((sprite.Width / sprFrames.Value) *i).ToString() + ",0," + (sprite.Width / sprFrames.Value).ToString() + "," + sprite.Height.ToString() + ",0,0);";
                    }
                    finaltxt += "surface_free(tempsurface);";
                    sprite.Dispose();
                    savefile.Filter = "all (*.*)|*.*";
                    savefile.FilterIndex = 1;
                    savefile.RestoreDirectory = true;
                    if (savefile.ShowDialog() == DialogResult.OK)
                    {
                        File.WriteAllText(savefile.FileName,finaltxt);
                    }
                    progressBar1.Value = progressBar1.Maximum;
                    Console.WriteLine(logtoconsole.Checked ? "everything done" : null);
                    donetxt.Text = "done lol";
                    finaltxt = "";
                }
                catch (SecurityException ex)
                {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Console.WriteLine("started");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = checkBox1.Checked;
        }
    }
}
