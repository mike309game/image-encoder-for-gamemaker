﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Security;
using System.Text;
using System.Windows.Forms;
namespace encoderthingform
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.filediag = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.sprname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.savefile = new System.Windows.Forms.SaveFileDialog();
            this.donetxt = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.realHeight = new System.Windows.Forms.NumericUpDown();
            this.realWidth = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.sprFrames = new System.Windows.Forms.NumericUpDown();
            this.sprX = new System.Windows.Forms.NumericUpDown();
            this.sprY = new System.Windows.Forms.NumericUpDown();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.realHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.realWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprFrames)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprY)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(235, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "do the thing";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.dothing);
            // 
            // filediag
            // 
            this.filediag.FileName = "filediag";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "sprite name";
            // 
            // sprname
            // 
            this.sprname.Location = new System.Drawing.Point(12, 25);
            this.sprname.Name = "sprname";
            this.sprname.Size = new System.Drawing.Size(100, 20);
            this.sprname.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "frames";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(118, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "xorigin";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(118, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "yorigin";
            // 
            // donetxt
            // 
            this.donetxt.AutoSize = true;
            this.donetxt.Location = new System.Drawing.Point(232, 113);
            this.donetxt.Name = "donetxt";
            this.donetxt.Size = new System.Drawing.Size(28, 13);
            this.donetxt.TabIndex = 9;
            this.donetxt.Text = "bruh";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.realHeight);
            this.groupBox1.Controls.Add(this.realWidth);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(12, 113);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // realHeight
            // 
            this.realHeight.Location = new System.Drawing.Point(44, 45);
            this.realHeight.Maximum = new decimal(new int[] {
            -1,
            -1,
            -1,
            0});
            this.realHeight.Name = "realHeight";
            this.realHeight.Size = new System.Drawing.Size(99, 20);
            this.realHeight.TabIndex = 16;
            // 
            // realWidth
            // 
            this.realWidth.Location = new System.Drawing.Point(44, 16);
            this.realWidth.Maximum = new decimal(new int[] {
            -1,
            -1,
            -1,
            0});
            this.realWidth.Name = "realWidth";
            this.realWidth.Size = new System.Drawing.Size(100, 20);
            this.realWidth.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "height";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "width";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(12, 90);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(145, 17);
            this.checkBox1.TabIndex = 12;
            this.checkBox1.Text = "override width and height";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // sprFrames
            // 
            this.sprFrames.Location = new System.Drawing.Point(12, 64);
            this.sprFrames.Maximum = new decimal(new int[] {
            -1,
            -1,
            -1,
            0});
            this.sprFrames.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.sprFrames.Name = "sprFrames";
            this.sprFrames.Size = new System.Drawing.Size(100, 20);
            this.sprFrames.TabIndex = 13;
            this.sprFrames.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // sprX
            // 
            this.sprX.Location = new System.Drawing.Point(118, 25);
            this.sprX.Maximum = new decimal(new int[] {
            -1,
            -1,
            -1,
            0});
            this.sprX.Name = "sprX";
            this.sprX.Size = new System.Drawing.Size(100, 20);
            this.sprX.TabIndex = 14;
            // 
            // sprY
            // 
            this.sprY.Location = new System.Drawing.Point(118, 64);
            this.sprY.Maximum = new decimal(new int[] {
            -1,
            -1,
            -1,
            0});
            this.sprY.Name = "sprY";
            this.sprY.Size = new System.Drawing.Size(100, 20);
            this.sprY.TabIndex = 15;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 225);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(305, 23);
            this.progressBar1.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 260);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.sprY);
            this.Controls.Add(this.sprX);
            this.Controls.Add(this.sprFrames);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.donetxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.sprname);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Encoder";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.realHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.realWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprFrames)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sprY)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog filediag;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox sprname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.SaveFileDialog savefile;
        private System.Windows.Forms.Label donetxt;
        //private System.Windows.Forms.CheckBox logtoconsole;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.NumericUpDown sprFrames;
        private System.Windows.Forms.NumericUpDown sprX;
        private System.Windows.Forms.NumericUpDown sprY;
        private System.Windows.Forms.NumericUpDown realWidth;
        private System.Windows.Forms.NumericUpDown realHeight;
        private ProgressBar progressBar1;
    }
}

